def xor(a, b)
  a ^ b
end

# a, b - two equal length byte arrays
# Returns a byte array of the XOR'ed inputs
def fixed_xor(a, b)
  type_err = "fixed_xor requires byte arrays as arguments"
  raise type_err unless a.kind_of?(Enumerable) && b.kind_of?(Enumerable)

  len_err = "inputs must be equal, fixed lengths"
  raise len_err unless a.length === b.length

  xored = []
  a.each_with_index do |byte, index|
    xored.push(xor(byte, b[index]))
  end

  xored
end

# text - array of bytes to be XOR'ed
# k - single character key (as a byte array with length 1)
# Returns a byte array
def single_char_xor(text, k)
  text_err = "Text should be a byte array"
  raise text_err unless text.kind_of?(Enumerable)

  key_err = "Key should be a single character byte array"
  raise key_err unless k.kind_of?(Enumerable) && k.length === 1

  text.map { |b| xor(b, k[0]) }
end