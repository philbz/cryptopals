require "base64"

# Starting with binary
def bin_to_hex(s)
  s.each_byte.map { |b| b.to_s(16) }.join
end

def bin_to_base64(s)
  Base64.strict_encode64(s)
end

# Starting with hexadecimal
def hex_to_bin(s)
  s.strip.scan(/../).map { |x| x.hex }.pack('C*')
end

# Starting with base64
def base64_to_bin(s)
  Base64.strict_decode64(s)
end
