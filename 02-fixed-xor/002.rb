require_relative "../lib/convert_encoding"
require_relative "../lib/xor"

first = '1c0111001f010100061a024b53535009181c'
second = '686974207468652062756c6c277320657965'
expected = '746865206b696420646f6e277420706c6179'

first_bytes = hex_to_bin(first).bytes
second_bytes = hex_to_bin(second).bytes

result = fixed_xor(first_bytes, second_bytes).map { |byte| byte.to_s(16) }.join

is_correct = result == expected

puts "======="
puts result
puts expected
puts "======="

raise "Nope" unless is_correct
puts "Yep!" if is_correct

# Not part of the challenge, but printing out what the resulting string says:
puts "\n"
puts hex_to_bin(result)